import React, { createContext, useContext, useState, useMemo, FC, ReactNode, Context, Dispatch } from 'react';

type State<T> = {
  state?: T;
  [key: string]: (() => void) | T | undefined;
};

type Setters<T> = (set: Dispatch<React.SetStateAction<T>>) => {
  [key: string]: (...args: any[]) => void;
};

type StoreProps<T> = {
  initialState?: T;
  setters: Setters<T>;
};

type Store<T> = {
  Context: Context<State<T>>;
  Provider: FC<{ children: ReactNode }>;
};

type StoreMap = {
  [key: string]: Store<unknown>;
};

type ProviderProps = {
  store: StoreMap;
  children: ReactNode;
};

const StateContext = createContext<StoreMap>({});

export function defineStore<T>({ initialState = {} as T, setters }: StoreProps<T>): Store<T> {
  const Context = createContext<State<T>>({ state: initialState });

  const Provider: FC<{ children: ReactNode }> = ({ children }) => {
    const [state, setState] = useState<T>(initialState);
    const value = useMemo(() => ({ state, ...setters(setState) }), [state]);
    return <Context.Provider value={value}>{children}</Context.Provider>;
  };

  return {
    Context,
    Provider,
  };
}

function buildProviderTree(providers: FC<{ children: ReactNode }>[]): FC<{ children: ReactNode }> {
  if (providers.length === 1) {
    return providers[0];
  }

  const A = providers.shift()!;
  const B = providers.shift()!;
  if (typeof A !== 'function' || typeof B !== 'function') {
    throw new Error('ProviderTree is not a valid React component');
  }

  return buildProviderTree([
    ({ children }) => (
      <A>
        <B>{children}</B>
      </A>
    ),
    ...providers,
  ]);
}

export function PureProvider({ store, children }: ProviderProps): JSX.Element {
  const providers: FC<{ children: ReactNode }>[] = [];
  Object.keys(store).forEach((name) => {
    providers.push(store[name].Provider);
  });
  const ProviderTree = buildProviderTree(providers);

  return (
    <ProviderTree>
      <StateContext.Provider value={store}>{children}</StateContext.Provider>
    </ProviderTree>
  );
}

export function useStore<T>(storeName: string): State<T> {
  if (!storeName) throw new Error('store name is required in useStore');
  const store = useContext(StateContext);
  if (!store) throw new Error('no passed stores');
  const state = useContext(store[storeName].Context) as State<T>;
  return state;
}